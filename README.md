# GitLab CE
Bastille template to bootstrap GitLab CE

## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/gitlab-ce/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/gitlab-ce/commits/master)

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/gitlab-ce
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/gitlab-ce
```
